import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/screenutil.dart';

import 'ui/common/utils/app_theme.dart';

import 'ui/common/utils/routes.dart';

void main() {
  String initialRoute = RouteName.root;

  print("reach main");
  runApp(ScanuttApp(initialRoute));
}

class ScanuttApp extends StatefulWidget {
  final String initialRoute;

  ScanuttApp(this.initialRoute);

  @override
  State<StatefulWidget> createState() {
    return ScanuttAppState();
  }
}

class ScanuttAppState extends State<ScanuttApp> {
  //final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    //AppComponentBase.getInstance().initialiseNetworkManager();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    //  FirebaseApp.initializeApp(context);
    return AppTheme(
        child: MaterialApp(
          initialRoute: widget.initialRoute,
          routes: Routes.baseRoutes,
          /* onGenerateRoute: (settings) {
        return FadePageRouteBuilder(
            page: Routes.baseRoutes[settings.name](context),
            settings: settings);
      },*/
          builder: (context, widget) {
            final _appTheme = AppTheme.of(context);
            ScreenUtil.init(context,
                width: _appTheme.expectedDeviceWidth,
                height: _appTheme.expectedDeviceHeight,
                allowFontScaling: false);
            return Stack(
              children: <Widget>[
                widget,
                /* Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: StreamBuilder<bool>(
                  initialData: true,
                  stream: AppComponentBase.getInstance()
                      .getNetworkManager()
                      .networkStream,
                  builder: (context, snapshot) {
                    return AnimatedContainer(
                        decoration: _appTheme.gradientBackground,
                        height: !snapshot.data
                            ? _appTheme.getResponsiveHeight(80)
                            : 0,
                        duration: Utils.animationDuration,
                        child: Material(
                          type: MaterialType.transparency,
                          child: Center(
                              child: Text(StringConstants.noInternetConnection,
                                  style: _appTheme.noInternetTextStyle)),
                        ));
                  }),
            ),*/
              ],
            );
          },
        ));
  }

  @override
  void dispose() {
    //AppComponentBase.getInstance().getNetworkManager().disposeStream();
    super.dispose();
  }
}



