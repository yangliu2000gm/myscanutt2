import 'dart:async';

import 'package:myscanutt2/ui/common/utils/app_images.dart';
import 'package:myscanutt2/ui/common/utils/app_theme.dart';
import 'package:myscanutt2/ui/common/utils/routes.dart';
import 'package:myscanutt2/ui/common/utils/strings.dart';
import 'package:myscanutt2/ui/common/utils/utils.dart';
import 'package:myscanutt2/ui/common/widgets/loading_overlay.dart';
import 'package:myscanutt2/ui/intro_page_1/intro_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shape_of_view/shape_of_view.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> with LoadingOverlay {
  AppThemeState _appTheme;
  // IntroBloc _sliderIntroBloc = IntroBloc();
  final _loadingStreamController = StreamController<bool>();
  int _current = 0;
  var padding;

  var mediaQuerySize;
  List<Widget> imageSliders;

  @override
  void dispose() {
    super.dispose();
    _loadingStreamController.close();
  }

  @override
  void initState() {
    super.initState();
    _loadingStreamController.stream.listen((onData) {
      showLoadingPage(onData);
    });
  }

  @override
  Widget build(BuildContext context) {
    _appTheme = AppTheme.of(context);
    padding = MediaQuery.of(context).padding;
    mediaQuerySize = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        Utils.getBackgroundImage(mediaQuerySize),
        //getBackgroundOpacity(),
        NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (OverscrollIndicatorNotification overscroll) {
              overscroll.disallowGlow();
              return false;
            },
            child: SingleChildScrollView(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: _appTheme.getResponsiveHeight(133),
                ),
                Utils.getScanuttImage(_appTheme, AppImages.scanuttHelloUncurl),
                SizedBox(height: _appTheme.getResponsiveHeight(7)),
                getIamRectangle(),
              ],
            )))
      ],
    );
  }

  Widget getIamRectangle() {
    return Align(
      alignment: Alignment.center,
      child: ShapeOfView(
        shape: BubbleShape(
          position: BubblePosition.Top,
          arrowPositionPercent: 0.5,
          borderRadius: _appTheme.getResponsiveWidth(100),
          arrowHeight: _appTheme.getResponsiveHeight(22),
          arrowWidth: _appTheme.getResponsiveWidth(20),
        ),
        child: Container(
          height: _appTheme.getResponsiveHeight(270),
          width: mediaQuerySize.width * 0.72,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: _appTheme.getResponsiveHeight(77),
              ),
              getIamFromLabel(),
              //getThankYouLabel(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(18),
              ),
              getNextButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget getNextButton() {
    return GestureDetector(
      onTap: () {
        Navigator.of(context, rootNavigator: false)
            .pushNamed(RouteName.introPage2);
      },
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: _appTheme.getResponsiveHeight(15)),
            height: _appTheme.getResponsiveHeight(50),
            width: _appTheme.getResponsiveWidth(230),
            child: Image.asset(
              AppImages.buttonShadow,
              fit: BoxFit.fill,
            ),
          ),
          Container(
            height: _appTheme.getResponsiveHeight(50),
            width: _appTheme.getResponsiveWidth(230),
            decoration: _appTheme.gradientThreeColorButtonBackground,
            child: Center(
              child: Text(
                StringConstants.next,
                style: _appTheme.white20RegMonstTS,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getIamFromLabel() {
    return Container(
      width: _appTheme.getResponsiveWidth(205),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          children: <TextSpan>[
            TextSpan(
              text: StringConstants.iAmFrom,
              style: _appTheme.lightGray14RegQSTS,
            ),
            TextSpan(
                text: StringConstants.planet,
                style: _appTheme.orange14BoldQSTS),
            TextSpan(
                text: StringConstants.and, style: _appTheme.lightGray14RegQSTS),
            TextSpan(
                text: StringConstants.you, style: _appTheme.orange14BoldQSTS),
          ],
        ),
      ),
    );
  }
}
