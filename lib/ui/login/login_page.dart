import 'dart:async';
import 'dart:convert';

import 'package:myscanutt2/ui/common/utils/app_images.dart';
import 'package:myscanutt2/ui/common/utils/app_theme.dart';
import 'package:myscanutt2/ui/common/utils/routes.dart';
import 'package:myscanutt2/ui/common/utils/strings.dart';
import 'package:myscanutt2/ui/common/utils/utils.dart';
import 'package:myscanutt2/ui/common/widgets/loading_overlay.dart';
import 'package:myscanutt2/ui/login/login_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_facebook_login/flutter_facebook_login.dart';
// import 'package:google_sign_in/google_sign_in.dart';
// import "package:http/http.dart" as http;
// import 'package:roundcheckbox/roundcheckbox.dart';
import 'package:shape_of_view/shape_of_view.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with LoadingOverlay {
  AppThemeState _appTheme;
  // LoginBloc _loginBloc = LoginBloc();
  final _loadingStreamController = StreamController<bool>();
  var padding;
  var mediaQuerySize;
  // GoogleSignInAccount _currentUser;
  // GoogleSignIn _googleSignIn = GoogleSignIn(
  //   scopes: <String>[
  //     'email',
  //   ],
  // );

  @override
  void dispose() {
    super.dispose();
    _loadingStreamController.close();
  }

  @override
  void initState() {
    super.initState();
    _loadingStreamController.stream.listen((onData) {
      showLoadingPage(onData);
    });

    // _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
    //   setState(() {
    //     _currentUser = account;
    //     print("_googleSignIn");
    //     print(account);
    //   });
    //   if (_currentUser != null) {
    //     /*_signBloc.userGoogleSocialRegistration(
    //         context, _loadingStreamController, _currentUser);*/
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    _appTheme = AppTheme.of(context);
    padding = MediaQuery.of(context).padding;
    mediaQuerySize = MediaQuery.of(context).size;

    return Stack(
      children: <Widget>[
        Utils.getBackgroundImage(mediaQuerySize),
        NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (OverscrollIndicatorNotification overscroll) {
              overscroll.disallowGlow();
              return false;
            },
            child: SingleChildScrollView(
                child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: _appTheme.getResponsiveHeight(150),
                  ),
                  Utils.getScanuttImage(
                      _appTheme, AppImages.scanuttHelloUncurl),
                  getPleaseLoginRectangle(),
                  SizedBox(height: _appTheme.getResponsiveHeight(21)),
                  getLoginRectangle(),
                  SizedBox(height: _appTheme.getResponsiveHeight(30)),
                ],
              ),
            )))
      ],
    );
  }

  Widget getBackgroundOpacity() {
    return Container(
      height: mediaQuerySize.height,
      width: mediaQuerySize.width,
      color: Colors.black.withOpacity(0.55),
    );
  }

  Widget getBackgroundImage() {
    return Container(
        height: mediaQuerySize.height,
        width: mediaQuerySize.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.backgroundImage), fit: BoxFit.cover),
        ));
  }

  Widget getScanuttImage() {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          width: _appTheme.getResponsiveWidth(203),
          height: _appTheme.getResponsiveWidth(204),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _appTheme.whiteColor.withOpacity(0.5)),
        ),
        Container(
          width: _appTheme.getResponsiveWidth(180),
          height: _appTheme.getResponsiveWidth(180),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _appTheme.whiteColor.withOpacity(0.93)),
        ),
        Container(
          width: _appTheme.getResponsiveWidth(146),
          height: _appTheme.getResponsiveWidth(157),
          child: Image.asset(
            AppImages.scannut,
            fit: BoxFit.fill,
          ),
        ),
      ],
    );
  }

  Widget getLoginRectangle() {
    return Stack(
      children: [
        Container(
            decoration: new BoxDecoration(
              borderRadius: new BorderRadius.circular(10.0),
              color: Colors.white.withOpacity(0.26),
              border: Border.all(
                  width: 20.0, color: Colors.white.withOpacity(0.26)),
            ),
            height: _appTheme.getResponsiveHeight(272),
            width: mediaQuerySize.width * 0.72,
            child: Container(color: Colors.white.withOpacity(0.26),)
        ),
        Container(
          margin: EdgeInsets.only(top: _appTheme.getResponsiveHeight(15),
              left: _appTheme.getResponsiveHeight(15)),
          height: _appTheme.getResponsiveHeight(244),
          width: mediaQuerySize.width * 0.65,
          decoration: new BoxDecoration(
            color: _appTheme.whiteColor,
            borderRadius: new BorderRadius.circular(10.0),
            border: Border.all(width: 1.0, color: Colors.white),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: _appTheme.getResponsiveHeight(20),
              ),
              getFacebookButton(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(17),
              ),
              getGoogleButton(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(26),
              ),
              // getTermsAndConditionCB(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(10),
              ),
              // getCovidTermsCB(),
            ],
          ),
        ),
      ],
    );
  }

  Widget getFacebookButton() {
    return GestureDetector(
      onTap: () {
        //_handleFbLogin();
        Navigator.of(context, rootNavigator: false)
            .pushNamed(RouteName.birthdayInfoPage);
      },
      child: Container(
        width: _appTheme.getResponsiveWidth(207),
        height: _appTheme.getResponsiveHeight(34),
        child: Image.asset(
          AppImages.fbButton,
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }

  // void _handleFbLogin() async {
  //   final facebookLogin = FacebookLogin();
  //   facebookLogin.loginBehavior = FacebookLoginBehavior.nativeWithFallback;
  //   final result = await facebookLogin.logIn(['email']);
  //   switch (result.status) {
  //     case FacebookLoginStatus.loggedIn:
  //       final token = result.accessToken.token;
  //       final graphResponse = await http.get(
  //           'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${token}');
  //       final Map<String, dynamic> profile = json.decode(graphResponse.body);
  //       print(profile);
  //       /*_signBloc.userFacebookSocialRegistration(
  //           context, _loadingStreamController, profile);*/
  //       break;
  //     case FacebookLoginStatus.cancelledByUser:
  //       break;
  //     case FacebookLoginStatus.error:
  //       break;
  //   }
  // }

  Widget getGoogleButton() {
    return GestureDetector(
      onTap: () {
        // _handleGoogleLogin();
        Navigator.of(context, rootNavigator: false)
            .pushNamed(RouteName.birthdayInfoPage);
      },
      child: Container(
        width: _appTheme.getResponsiveWidth(207),
        height: _appTheme.getResponsiveHeight(34),
        child: Image.asset(
          AppImages.googleButton,
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }

  // Future<void> _handleGoogleLogin() async {
  //   try {
  //     await _googleSignIn.signIn();
  //   } catch (error) {
  //     print(error);
  //   }
  // }

  // Widget getTermsAndConditionCB() {
  //   return Container(
  //     width: _appTheme.getResponsiveWidth(207),
  //     child: StreamBuilder<bool>(
  //         stream: _loginBloc.agreeTermsAndConditions.stream,
  //         builder: (context, snapshot) {
  //           return Row(
  //             children: <Widget>[
  //               RoundCheckBox(
  //                 onTap: (selected) {
  //                   _loginBloc.agreeTermsAndConditions.sink.add(selected);
  //                 },
  //                 checkedWidget: Icon(
  //                   Icons.check,
  //                   color: Colors.white,
  //                   size: _appTheme.getResponsiveWidth(17),
  //                 ),
  //                 borderColor: _appTheme.brownColor,
  //                 checkedColor: _appTheme.primaryColor,
  //                 size: _appTheme.getResponsiveWidth(23),
  //               ),
  //               SizedBox(
  //                 width: _appTheme.getResponsiveWidth(9),
  //               ),
  //               Flexible(
  //                 child: Text(
  //                   StringConstants.agreeTerms,
  //                   style: _appTheme.gray12RegQSTS,
  //                 ),
  //               )
  //             ],
  //           );
  //         }),
  //   );
  // }

  Widget getPleaseLoginRectangle() {
    return ShapeOfView(
      shape: BubbleShape(
        position: BubblePosition.Top,
        arrowPositionPercent: 0.5,
        borderRadius: _appTheme.getResponsiveWidth(100),
        arrowHeight: _appTheme.getResponsiveHeight(22),
        arrowWidth: _appTheme.getResponsiveWidth(20),
      ),
      child: Container(
        height: _appTheme.getResponsiveHeight(126),
        width: mediaQuerySize.width * 0.72,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: _appTheme.getResponsiveHeight(30),
            ),
            getPleaseLoginLabel(),
          ],
        ),
      ),
    );
  }

  // Widget getCovidTermsCB() {
  //   return Container(
  //     width: _appTheme.getResponsiveWidth(207),
  //     child: StreamBuilder<bool>(
  //         stream: _loginBloc.agreeCovidTerms.stream,
  //         builder: (context, snapshot) {
  //           return Row(
  //             children: <Widget>[
  //               RoundCheckBox(
  //                 onTap: (selected) {
  //                   _loginBloc.agreeCovidTerms.sink.add(selected);
  //                 },
  //                 checkedWidget: Icon(
  //                   Icons.check,
  //                   color: Colors.white,
  //                   size: _appTheme.getResponsiveWidth(17),
  //                 ),
  //                 borderColor: _appTheme.brownColor,
  //                 checkedColor: _appTheme.primaryColor,
  //                 size: _appTheme.getResponsiveWidth(23),
  //               ),
  //               SizedBox(
  //                 width: _appTheme.getResponsiveWidth(9),
  //               ),
  //               Flexible(
  //                 child: Text(
  //                   StringConstants.coivdTerms,
  //                   style: _appTheme.gray12RegQSTS,
  //                 ),
  //               )
  //             ],
  //           );
  //         }),
  //   );
  // }

  Widget getPleaseLoginLabel() {
    return Container(
      width: _appTheme.getResponsiveWidth(230),
      child: Text(StringConstants.pleaseLogin,
          textAlign: TextAlign.center, style: _appTheme.darkGray15BoldQSTS),
    );
  }
}
