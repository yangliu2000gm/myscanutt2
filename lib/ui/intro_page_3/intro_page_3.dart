import 'dart:async';

import 'package:myscanutt2/ui/common/utils/app_images.dart';
import 'package:myscanutt2/ui/common/utils/app_theme.dart';
import 'package:myscanutt2/ui/common/utils/routes.dart';
import 'package:myscanutt2/ui/common/utils/strings.dart';
import 'package:myscanutt2/ui/common/utils/utils.dart';
import 'package:myscanutt2/ui/common/widgets/loading_overlay.dart';
import 'package:myscanutt2/ui/intro_page_3/intro_bloc_3.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shape_of_view/shape_of_view.dart';

class IntroPage3 extends StatefulWidget {
  @override
  _IntroPage3State createState() => _IntroPage3State();
}

class _IntroPage3State extends State<IntroPage3> with LoadingOverlay {
  AppThemeState _appTheme;
  // IntroBloc3 _sliderIntroBloc = IntroBloc3();
  final _loadingStreamController = StreamController<bool>();
  int _current = 0;
  var padding;

  var mediaQuerySize;
  List<Widget> imageSliders;

  @override
  void dispose() {
    super.dispose();
    _loadingStreamController.close();
  }

  @override
  void initState() {
    super.initState();
    _loadingStreamController.stream.listen((onData) {
      showLoadingPage(onData);
    });
  }

  @override
  Widget build(BuildContext context) {
    _appTheme = AppTheme.of(context);
    padding = MediaQuery.of(context).padding;
    mediaQuerySize = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        Utils.getBackgroundImage(mediaQuerySize),
        //getBackgroundOpacity(),
        NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (OverscrollIndicatorNotification overscroll) {
              overscroll.disallowGlow();
              return false;
            },
            child: SingleChildScrollView(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: _appTheme.getResponsiveHeight(133),
                ),
                Utils.getScanuttImage(_appTheme, AppImages.scanuttHelloUncurl),
                SizedBox(height: _appTheme.getResponsiveHeight(7)),
                getMysteryBoxRectangle(),
              ],
            )))
      ],
    );
  }

  Widget getMysteryBoxRectangle() {
    return Align(
      alignment: Alignment.center,
      child: ShapeOfView(
        shape: BubbleShape(
          position: BubblePosition.Top,
          arrowPositionPercent: 0.5,
          borderRadius: _appTheme.getResponsiveWidth(100),
          arrowHeight: _appTheme.getResponsiveHeight(22),
          arrowWidth: _appTheme.getResponsiveWidth(20),
        ),
        child: Container(
          height: _appTheme.getResponsiveHeight(380),
          width: mediaQuerySize.width * 0.72,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: _appTheme.getResponsiveHeight(41),
              ),
              getGiftBoxImage(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(24),
              ),
              getByScanningLabel(),
              //getThankYouLabel(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(18),
              ),
              getLetsBeginButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget getBackgroundImage() {
    return Container(
        height: mediaQuerySize.height,
        width: mediaQuerySize.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.backgroundImage), fit: BoxFit.cover),
        ));
  }

  Widget getLetsBeginButton() {
    return GestureDetector(
      onTap: () {
        Navigator.of(context, rootNavigator: false)
            .pushNamed(RouteName.loginPage);
      },
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: _appTheme.getResponsiveHeight(15)),
            height: _appTheme.getResponsiveHeight(50),
            width: _appTheme.getResponsiveWidth(230),
            child: Image.asset(
              AppImages.buttonShadow,
              fit: BoxFit.fill,
            ),
          ),
          Container(
            height: _appTheme.getResponsiveHeight(50),
            width: _appTheme.getResponsiveWidth(230),
            decoration: _appTheme.gradientThreeColorButtonBackground,
            child: Center(
              child: Text(
                StringConstants.letsBegin,
                style: _appTheme.white16BoldMonstTS,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getGiftBoxImage() {
    return Container(
      height: _appTheme.getResponsiveWidth(101),
      width: _appTheme.getResponsiveWidth(101),
      child: Image.asset(
        AppImages.giftBox,
        fit: BoxFit.fill,
      ),
    );
  }

  Widget getByScanningLabel() {
    return Container(
      width: _appTheme.getResponsiveWidth(205),
      child: Text(
        StringConstants.byScanning,
        style: _appTheme.lightGray14RegQSTS,
        textAlign: TextAlign.center,
      ),
    );
  }
}
