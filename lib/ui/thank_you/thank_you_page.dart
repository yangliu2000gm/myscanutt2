import 'dart:async';

import 'package:myscanutt2/ui/common/utils/app_images.dart';
import 'package:myscanutt2/ui/common/utils/app_theme.dart';
import 'package:myscanutt2/ui/common/utils/routes.dart';
import 'package:myscanutt2/ui/common/utils/strings.dart';
import 'package:myscanutt2/ui/common/utils/utils.dart';
import 'package:myscanutt2/ui/common/widgets/loading_overlay.dart';
import 'package:myscanutt2/ui/thank_you/thank_you_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shape_of_view/shape_of_view.dart';

class ThankYouPage extends StatefulWidget {
  @override
  _ThankYouPageState createState() => _ThankYouPageState();
}

class _ThankYouPageState extends State<ThankYouPage> with LoadingOverlay {
  AppThemeState _appTheme;
  // ThankYouBloc _thankYouBloc = ThankYouBloc();
  final _loadingStreamController = StreamController<bool>();
  var padding;
  var mediaQuerySize;

  @override
  void dispose() {
    super.dispose();
    _loadingStreamController.close();
  }

  @override
  void initState() {
    super.initState();
    _loadingStreamController.stream.listen((onData) {
      showLoadingPage(onData);
    });
  }

  @override
  Widget build(BuildContext context) {
    _appTheme = AppTheme.of(context);
    padding = MediaQuery.of(context).padding;
    mediaQuerySize = MediaQuery.of(context).size;

    return Stack(
      children: <Widget>[
        Utils.getBackgroundImage(mediaQuerySize),
        //getBackgroundOpacity(),
        NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (OverscrollIndicatorNotification overscroll) {
              overscroll.disallowGlow();
              return false;
            },
            child: SingleChildScrollView(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: _appTheme.getResponsiveHeight(133),
                ),
                Utils.getScanuttImage(_appTheme, AppImages.scanuttHello),
                SizedBox(height: _appTheme.getResponsiveHeight(7)),
                getThankYouRectangle(),
              ],
            )))
      ],
    );
  }

  Widget getScanuttLabel() {
    return Align(
      alignment: Alignment.center,
      child: Container(
          width: _appTheme.getResponsiveWidth(302),
          child: Text(
            StringConstants.wantToKnow,
            style: _appTheme.white20RegularTS,
            textAlign: TextAlign.center,
          )),
    );
  }

  Widget getThankYouRectangle() {
    return Align(
      alignment: Alignment.center,
      child: ShapeOfView(
        shape: BubbleShape(
          position: BubblePosition.Top,
          arrowPositionPercent: 0.5,
          borderRadius: _appTheme.getResponsiveWidth(100),
          arrowHeight: _appTheme.getResponsiveHeight(22),
          arrowWidth: _appTheme.getResponsiveWidth(20),
        ),
        child: Container(
          height: _appTheme.getResponsiveHeight(226),
          width: mediaQuerySize.width * 0.72,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: _appTheme.getResponsiveHeight(51),
              ),
              getHiLabel(),
              getThankYouLabel(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(18),
              ),
              getNextButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget getHiLabel() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
            text: StringConstants.hiIAm,
            style: _appTheme.lightGray16RegQSTS,
          ),
          TextSpan(
              text: StringConstants.scanutt, style: _appTheme.pink18BoldQSTS),
        ],
      ),
    );
  }

  Widget getNextButton() {
    return GestureDetector(
      onTap: () {
        Navigator.of(context, rootNavigator: false)
            .pushNamed(RouteName.introPage);
      },
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: _appTheme.getResponsiveHeight(15)),
            height: _appTheme.getResponsiveHeight(50),
            width: _appTheme.getResponsiveWidth(230),
            child: Image.asset(
              AppImages.buttonShadow,
              fit: BoxFit.fill,
            ),
          ),
          Container(
            height: _appTheme.getResponsiveHeight(50),
            width: _appTheme.getResponsiveWidth(230),
            decoration: _appTheme.gradientThreeColorButtonBackground,
            child: Center(
              child: Text(
                StringConstants.next,
                style: _appTheme.white20RegMonstTS,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getThankYouLabel() {
    return Text(StringConstants.thankYou,
        textAlign: TextAlign.center, style: _appTheme.lightGray14RegQSTS);
  }

  Widget getSkipLabel() {
    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        height: _appTheme.getResponsiveHeight(30),
        width: _appTheme.getResponsiveWidth(84),
        child: Text(StringConstants.skip,
            textAlign: TextAlign.start, style: _appTheme.white20RegMonstTS),
      ),
    );
  }
}
