import 'package:myscanutt2/ui/common/utils/app_images.dart';
import 'package:myscanutt2/ui/common/utils/app_theme.dart';
import 'package:myscanutt2/ui/common/utils/strings.dart';
import 'package:myscanutt2/ui/empty_basket/empty_basket_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmptyBasketPage extends StatefulWidget {
  @override
  _EmptyBasketPageState createState() => _EmptyBasketPageState();
}

class _EmptyBasketPageState extends State<EmptyBasketPage> {
  // EmptyBasketBloc _emptyBasketBloc = EmptyBasketBloc();
  AppThemeState _appTheme;
  @override
  Widget build(BuildContext context) {
    _appTheme = AppTheme.of(context);
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        padding: EdgeInsets.only(
          top: _appTheme.getResponsiveHeight(56),
            left: _appTheme.getResponsiveWidth(36),
            right: _appTheme.getResponsiveWidth(36)),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            SizedBox(
              height: _appTheme.getResponsiveHeight(46),
            ),
            _getBasketListCard()
          ],
        ),
      ),
    );
  }

  _getBasketListCard() {
    return Stack(
      children: [
        _getTransparentBackgroundCard(),
        _getWhiteBackgroundCard(),
      ],
    );
  }

  Widget _getTransparentBackgroundCard() {
    return Container(
      width: _appTheme.getResponsiveWidth(303),
      height: _appTheme.getResponsiveHeight(503),
      decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.4),
          borderRadius: BorderRadius.circular(40.0)),
    );
  }

  _getBuyMoreButton() {
    return InkWell(
      onTap: () {},
      child: Container(
        height: _appTheme.getResponsiveHeight(33),
        width: _appTheme.getResponsiveWidth(148),
        decoration: _appTheme.gradientThreeColorButtonBackground,
        child: Center(
          child: Text(
            StringConstants.buyMore,
            style: _appTheme.white12BoldMonstTS,
          ),
        ),
      ),
    );
  }

  _getWhiteBackgroundCard() {
    return Container(
      margin: EdgeInsets.only(
          left: _appTheme.getResponsiveWidth(11),
          top: _appTheme.getResponsiveHeight(12)),
      width: _appTheme.getResponsiveWidth(281),
      height: _appTheme.getResponsiveHeight(479),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(40.0)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: _appTheme.getResponsiveHeight(38),
          ),
          _getHungerImages(),
          SizedBox(
            height: _appTheme.getResponsiveHeight(17),
          ),
          _getEmptyText(),
          SizedBox(
            height: _appTheme.getResponsiveHeight(23),
          ),
          _getBuyMoreButton(),
        ],
      ),
    );
  }

  _getEmptyText() {
    return Container(
      width: _appTheme.getResponsiveHeight(199),
      child: Text(StringConstants.buyFood,style: _appTheme.darkGray14BoldQSTS,textAlign: TextAlign.center,),
    );
  }
  
  _getHungerImages() {
    return Container(
      child: Image.asset(AppImages.hungerScanutt),
      width: _appTheme.getResponsiveWidth(179),
      height: _appTheme.getResponsiveHeight(223),
      padding: EdgeInsets.fromLTRB(_appTheme.getResponsiveWidth(10), _appTheme.getResponsiveHeight(5), _appTheme.getResponsiveWidth(10), _appTheme.getResponsiveHeight(5)),
    );
  }
}
