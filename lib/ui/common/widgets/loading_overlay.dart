import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';

import 'custom_cupertion_activity_indicator.dart';

mixin LoadingOverlay<T extends StatefulWidget> on State<T> {
  OverlayEntry loadingOverlay;
  double _opacity = 0.99;

  OverlayEntry _buildOverlayEntry() {
    Size size = MediaQuery.of(context).size;
    return OverlayEntry(builder: (context) {
      return Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          color: Colors.transparent,
          height: size.height,
          width: size.width,
          alignment: Alignment.center,
          child: Center(
              child: Container(
            alignment: Alignment.center,
            height: 120,
            width: 120,
            decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.85),
                shape: BoxShape.rectangle,
                borderRadius: new BorderRadius.all(
                  const Radius.circular(10),
                )),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CustomCupertinoActivityIndicator(),
                SizedBox(height: 20),
                Text("Loading",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 12,
                        color: Colors.white,
                        fontFamily: "montserrat")),
              ],
            ),
          )),
        ),
      );
    });
  }

  void showLoadingPage(bool isLoading) {
    try {
      if (isLoading) {
        Navigator.of(context, rootNavigator: true)
            .overlay
            .insert(loadingOverlay);
      } else {
        loadingOverlay?.remove();
      }
    } catch (error) {}
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((context) {
      loadingOverlay = _buildOverlayEntry();
    });
  }

  @override
  void dispose() {
    showLoadingPage(false);
    super.dispose();
  }
}
