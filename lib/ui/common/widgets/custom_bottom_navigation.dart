import 'package:myscanutt2/ui/common/utils/app_images.dart';
import 'package:myscanutt2/ui/common/utils/app_theme.dart';
import 'package:myscanutt2/ui/common/utils/strings.dart';
import 'package:flutter/material.dart';

/// [CustomNavigationBar] Parent class to create a custom navigation bar
class CustomNavigationBar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CustomNavigationBarState();
}

class _CustomNavigationBarState extends State<CustomNavigationBar> {
  AppThemeState _appTheme;
  var mediaQuerySize;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _appTheme = AppTheme.of(context);
    mediaQuerySize = MediaQuery.of(context).size;

    return Container(
      color: Colors.transparent,
      height: _appTheme.getResponsiveHeight(160),
      child: Container(
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: mediaQuerySize.width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(60.0),
                        topRight: const Radius.circular(60.0))),
                height: _appTheme.getResponsiveHeight(120),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _getHomeMenu(),
                    _getProfileMenu(),
                  ],
                ),
              ),
            ),
            _getScanMenu(),
          ],
        ),
      ),
    );
  }

  Widget _getHomeMenu() {
    return Container(
      margin: EdgeInsets.only(top: _appTheme.getResponsiveHeight(30)),
      width: _appTheme.getResponsiveWidth(150),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          _getHomeMenuIcon(),
          _getHomeMenuText(),
        ],
      ),
    );
  }

  Widget _getProfileMenu() {
    return Center(
      child: Container(
        margin: EdgeInsets.only(top: _appTheme.getResponsiveHeight(30)),
        width: _appTheme.getResponsiveWidth(150),
        child: Column(
          children: [
            _getProfileMenuIcon(),
            _getProfileMenuText(),
          ],
        ),
      ),
    );
  }

  Widget _getHomeMenuIcon() {
    return Container(
      height: _appTheme.getResponsiveWidth(30),
      width: _appTheme.getResponsiveWidth(30),
      child: Image.asset(
        AppImages.home,
        fit: BoxFit.fill,
      ),
    );
  }

  Widget _getProfileMenuText() {
    return Text(
      StringConstants.profile,
      style: _appTheme.gray14RegMonstTS,
    );
  }

  Widget _getProfileMenuIcon() {
    return Container(
      height: _appTheme.getResponsiveWidth(30),
      width: _appTheme.getResponsiveWidth(30),
      child: Image.asset(
        AppImages.profile,
        fit: BoxFit.fill,
      ),
    );
  }

  Widget _getHomeMenuText() {
    return Text(
      StringConstants.home,
      style: _appTheme.gray14RegMonstTS,
    );
  }

  Widget _getScanMenuText() {
    return Text(
      StringConstants.scanMe,
      style: _appTheme.gray14RegMonstTS,
    );
  }

  Widget _getScanMenuIcon() {
    return Container(
      padding: EdgeInsets.all(14),
      height: _appTheme.getResponsiveWidth(86),
      width: _appTheme.getResponsiveWidth(86),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: _appTheme.lightGrayColor.withOpacity(0.5),
            blurRadius: 2.0,
            spreadRadius: 1.0,
            offset: Offset(1.0, 1.0), // shadow direction: bottom right
          )
        ],
      ),
      child: Image.asset(
        AppImages.scan,
        fit: BoxFit.fill,
      ),
    );
  }

  Widget _getScanMenu() {
    return Align(
      alignment: Alignment.topCenter,
      child: Column(
        children: [
          _getScanMenuIcon(),
          SizedBox(
            height: _appTheme.getResponsiveHeight(5),
          ),
          _getScanMenuText(),
        ],
      ),
    );
  }
}
