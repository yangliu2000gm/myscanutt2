import 'package:myscanutt2/ui/common/utils/app_images.dart';
import 'package:myscanutt2/ui/common/utils/app_theme.dart';
import 'package:myscanutt2/ui/common/utils/routes.dart';
import 'package:flutter/material.dart';

class CustomAppbar extends StatefulWidget {
  final GlobalKey<ScaffoldState> drawerKey;
  final double height;
  final ValueChanged<BuildContext> onTapped;
  final GlobalKey<CustomAppbarState> key;

  CustomAppbar({this.drawerKey, this.height, this.onTapped, this.key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CustomAppbarState();
  }
}

class CustomAppbarState extends State<CustomAppbar> {
  AppThemeState _appTheme;
  var mediaQuerySize;
  String title = "";

  @override
  Widget build(BuildContext context) {
    return _buildAppbar(context);
  }

  Widget _buildAppbar(BuildContext context) {
    _appTheme = AppTheme.of(context);
    mediaQuerySize = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.only(
          top: _appTheme.getResponsiveHeight(29),
          left: _appTheme.getResponsiveWidth(36),
          right: _appTheme.getResponsiveWidth(36)),
      width: mediaQuerySize.width,
      color: Colors.transparent,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          title == RouteName.homePage || title.isEmpty
              ? _getNavigationIcon()
              : getBackButtonWithTitle(),
          _getTotalCoinsWidget('265'),
        ],
      ),
    );
  }

  _getNavigationIcon() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: _appTheme.getResponsiveWidth(47),
          height: _appTheme.getResponsiveHeight(5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: _appTheme.whiteColor,
          ),
        ),
        SizedBox(
          height: _appTheme.getResponsiveHeight(3),
        ),
        Container(
          width: _appTheme.getResponsiveWidth(38),
          height: _appTheme.getResponsiveHeight(5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: _appTheme.whiteColor,
          ),
        ),
        SizedBox(
          height: _appTheme.getResponsiveHeight(3),
        ),
        Container(
          width: _appTheme.getResponsiveWidth(30),
          height: _appTheme.getResponsiveHeight(5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: _appTheme.whiteColor,
          ),
        )
      ],
    );
  }

  _getTotalCoinsWidget(String totalCoins) {
    return Row(
      children: [
        _getTotalCoinsTextWidget(totalCoins),
        SizedBox(
          width: _appTheme.getResponsiveWidth(5),
        ),
        _getTotalCoinsImageWidget()
      ],
    );
  }

  _getTotalCoinsTextWidget(String totalCoins) {
    return Container(
      child: Text(
        totalCoins,
        style: _appTheme.white21RegMonstTS,
      ),
    );
  }

  void updateTitle(String title) {
    setState(() {
      this.title = title;
    });
  }

  Widget _getTotalCoinsImageWidget() {
    return Container(child: Image.asset(AppImages.dollerIcon));
  }

  Widget getBackButtonWithTitle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(
          Icons.arrow_back,
          color: Colors.white,
          size: _appTheme.getResponsiveHeight(20),
        ),
        SizedBox(
          width: _appTheme.getResponsiveWidth(10),
        ),
        Text(
          title,
          style: _appTheme.white16BoldQSTS,
        )
      ],
    );
  }
}
