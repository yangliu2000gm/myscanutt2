//import 'package:myscanutt/ui/basket/basket_page.dart';
//import 'package:myscanutt/ui/birthday_info/birthday_info_page.dart';
import 'package:myscanutt2/ui/dashboard/dashboard_page.dart';
import 'package:myscanutt2/ui/empty_basket/empty_basket_page.dart';
import 'package:myscanutt2/ui/home_page/home_page.dart';
import 'package:myscanutt2/ui/intro_page_1/intro_page.dart';
import 'package:myscanutt2/ui/intro_page_2/intro_page_2.dart';
import 'package:myscanutt2/ui/intro_page_3/intro_page_3.dart';
//import 'package:Scanutt/ui/level_1/page_level_1.dart';
import 'package:myscanutt2/ui/login/login_page.dart';
import 'package:myscanutt2/ui/thank_you/thank_you_page.dart';
import 'package:flutter/material.dart';

class RouteName {
  // Base routes
  static final String root = "/";
  static final String birthdayInfoPage = "/BirthdayInfoPage";
  static final String loginPage = "/LoginPage";
  static final String thankYouPage = "/ThankYouPage";
  static final String introPage = "/IntroPage";
  static final String introPage2 = "/IntroPage2";
  static final String introPage3 = "/IntroPage3";
  static final String dashBoardPage = "/DashBoardPage";
  static final String homePage = "/HomePage";
  static final String scanuttStatusPage = "/ScanuttStatusPage";
  static final String basketPage = "/BasketPage";
  static final String level1Page = "/Level1Page";
  static final String emptyBasketPage = "/EmptyBasketPage";
}

class Routes {
  static final baseRoutes = <String, WidgetBuilder>{
    RouteName.root: (context) => ThankYouPage(),
    RouteName.introPage: (context) => IntroPage(),
    RouteName.introPage2: (context) => IntroPage2(),
    RouteName.introPage3: (context) => IntroPage3(),
    // RouteName.birthdayInfoPage: (context) => BirthdayInfoPage(),
    RouteName.loginPage: (context) => LoginPage(),
    RouteName.thankYouPage: (context) => ThankYouPage(),
    RouteName.dashBoardPage: (context) => DashboardPage(),
    RouteName.homePage: (context) => HomePage(),
  };
  static final commonRoutes = <String, Widget>{
    RouteName.homePage: HomePage(),
    // RouteName.basketPage: BasketPage(),
    // RouteName.scanuttStatusPage: ScanuttStatusPage(),
    // RouteName.level1Page: Level1Page(),
    // RouteName.emptyBasketPage: EmptyBasketPage(),
    // RouteName.scanuttStatusPage: ScanuttStatusPage(),
    // RouteName.emptyBasketPage: EmptyBasketPage(),
    // RouteName.moreCoinsPage: MoreCoinsPage(),
    // RouteName.particleStorePage: ParticleStorePage(),
    // RouteName.particleStorePage2: PartialStorePage2(),
    // RouteName.infinityPouch: InfinityPouchPage(),
    // RouteName.profilePage: ProfilePage(),
    // RouteName.faqPage: FAQPage(),
    // RouteName.gravityWallPage: GravityWallPage(),
    // RouteName.scan2Page: Scan2Page(),
    // RouteName.scanResultPage: ScanResultPage(),
    // RouteName.levelUpPage: LevelUpPage(),
    // RouteName.achievementPage2: AchievementPage2(),
    // RouteName.hungryCardDrag1: HungryCardDrag1Page(),
    // RouteName.personalDetails: PersonalDetailPage(),
    // RouteName.moreCoinPage: MoreCoinPage(),
    // RouteName.achievementCollectionPage: AchievementCollectionPage(),
    // RouteName.levelUp1: LevelUp1Page(),
    // RouteName.privacyPolicyPage: PrivacyPolicyPage(),
  };
}
