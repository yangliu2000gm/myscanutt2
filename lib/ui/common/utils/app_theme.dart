import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

///
/// This class contains all UI related styles
///
class AppTheme extends StatefulWidget {
  final Widget child;

  AppTheme({
    @required this.child,
  });

  @override
  State<StatefulWidget> createState() {
    return AppThemeState();
  }

  static AppThemeState of(BuildContext context) {
    final _InheritedStateContainer inheritedStateContainer =
        context.dependOnInheritedWidgetOfExactType();
    if (inheritedStateContainer == null) {
      return AppThemeState();
    } else {
      return inheritedStateContainer.data;
    }
  }
}

class AppThemeState extends State<AppTheme> {
  double getResponsiveFont(double value) => ScreenUtil().setSp(value);

  double getResponsiveWidth(double value) => ScreenUtil().setWidth(value);

  double getResponsiveHeight(double value) => ScreenUtil().setHeight(value);

  double squareBoxHWWithBorder = 121;
  double squareBoxHW = 120;

  ///
  /// Define All your colors here which are used in whole application
  ///
  Color get whiteColor => Color(0xFFFFFFFF);

  Color get notSelectedDot => Color(0xFF989898);

  Color get selectedDot => whiteColor;

  Color get lightGrayColor => Color(0xFF707070);

  Color get darkGrayColor => Color(0xFF484848);

  Color get primaryColor => Color(0xFFB31EED);

  Color get brownColor => Color(0xFF602424);

  Color get blueColor => Color(0xFF3E64FF);

  Color get redColor => Color(0xFFFB5951);

  Color get blackColor => Color(0xFF000000);

  Color get hungryScanuttTextColor => Color(0xffE04161);

  Color get loginStartGradient => Color(0xFFE8483C);

  Color get loginCenterGradient => Color(0xFFD4379E);

  Color get loginEndGradient => Color(0xFFB31EED);

  Color get cardEndGradient => Color(0xFFFFE259);

  Color get cardStartGradient => Color(0xFFFFA751);

  Color get cardParticleEndGradient => Color(0xFF6DD5ED);

  Color get cardParticleStartGradient => Color(0xFF2193B0);

  Color get cardInfinityPouchEndGradient => Color(0xFFFFA751);

  Color get cardInfinityPouchStartGradient => Color(0xFFF12711);

  Color get moreCoinsEndGradient => Color(0xFF8360C3);

  Color get moreCoinsStartGradient => Color(0xFF2EBF91);

  Color get notificationEndGradient => Color(0xFF3F2B96);

  Color get notificationStartGradient => Color(0xFFA8C0FF);

  Color get pinkFontColor => Color(0xFFD73990);

  Color get orangeFontColor => Color(0xFFE04161);

  Color get cardShadow => Color(0xFF000029);

  Color get sliderColor => Color(0xFFFB5951);

  Color get feedMeEndGradient => Color(0xFF8360C3);

  Color get feedMeCenterGradient => Color(0xFF2EBF91);

  Color get feedMeStartGradient => Color(0xFF2EBF91);

  Color get orangeStartGradient => Color(0xFFE49A10);

  Color get orangeEndGradient => Color(0xFFEB441D);

  Color get lightOrangeFontColor => Color(0xFFFF9B6A);

  Color get greenColor => Color(0xFFA1DD70);

  Color get unSelectedTextColor => Color(0xFFA8A8A8);

  Color get whitePopupBgColor => Color(0xFAF6FBFA);

  Color get dullWhiteColor => Color(0xFFF8EDED);

  Color get boxShadowColor => Color(0x00000029);

  Color get purpleColor => Color(0xFF911398);

  Color get yellowColor => Color(0xFFFCA40B);

  Color get lightPurpleColor => Color(0xffC55DE3);

  ///
  /// Mention height and width which are mentioned in your design file(i.e XD)
  /// to maintain ratio for all other devices
  ///
  num get expectedDeviceWidth => 375;

  num get expectedDeviceHeight => 817.18;

  ///
  /// List of your Text Styles which are used through this app.
  ///
  /// - asset: assets/fonts/BubbleboddyNeue-Bold.ttf
  ///          weight: 600
  ///        - asset: assets/fonts/BubbleboddyNeue-Regular.ttf
  ///          weight: 400
  ///        - asset: assets/fonts/BubbleboddyNeue-Thin.ttf
  ///          weight: 500
  ///

  TextStyle get noInternetTextStyle => TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: getResponsiveFont(20),
      fontFamily: "BubbleboddyNeue",
      color: whiteColor);

  TextStyle get white52BoldBBNTS => TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: getResponsiveFont(52),
      fontFamily: "BubbleboddyNeue",
      color: whiteColor);

  TextStyle get white14BoldStyle => TextStyle(
        fontWeight: FontWeight.w600,
        fontSize: getResponsiveFont(14),
        color: Colors.white,
        fontFamily: "BubbleboddyNeue",
      );

  TextStyle get white15BoldTS => TextStyle(
        fontWeight: FontWeight.w600,
        fontSize: getResponsiveFont(15),
        color: Colors.white,
        fontFamily: "BubbleboddyNeue",
      );

  TextStyle get white16RegularTS => TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(16),
        color: Colors.white,
        fontFamily: "BubbleboddyNeue",
      );

  TextStyle get white20RegularTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.none,
        fontSize: getResponsiveFont(20),
        color: Colors.white,
        fontFamily: "BubbleboddyNeue",
      );

  ///- family: montserrat
  ///      fonts:
  ///        - asset: assets/fonts/Montserrat-Bold.ttf
  ///          weight: 700
  ///        - asset: assets/fonts/Montserrat-Regular.ttf
  ///          weight: 400

  get gray14RegMonstTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.none,
        fontSize: getResponsiveFont(14),
        color: lightGrayColor,
        fontFamily: "montserrat",
      );

  get white14RegMonstTs =>
      TextStyle(
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.none,
        fontSize: getResponsiveFont(14),
        color: whiteColor,
        fontFamily: "montserrat",
      );

  get white16RegMonstTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.none,
        fontSize: getResponsiveFont(16),
        color: whiteColor,
        fontFamily: "montserrat",
      );

  get white15BoldMonstTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(15),
        color: Colors.white,
        fontFamily: "montserrat",
      );

  get white16BoldMonstTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(16),
        color: Colors.white,
        fontFamily: "montserrat",
      );

  TextStyle get gray12RegMonstTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.none,
        fontSize: getResponsiveFont(12),
        color: lightGrayColor,
        fontFamily: "montserrat",
      );

  TextStyle get white12BoldMonstTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        decoration: TextDecoration.none,
        fontSize: getResponsiveFont(12),
        color: whiteColor,
        fontFamily: "montserrat",
      );

  TextStyle get white12RegMonstTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.none,
        fontSize: getResponsiveFont(12),
        color: whiteColor,
        fontFamily: "montserrat",
      );

  TextStyle get white10BoldMonstTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        decoration: TextDecoration.none,
        fontSize: getResponsiveFont(10),
        color: whiteColor,
        fontFamily: "montserrat",
      );

  TextStyle get darkGray20RegMonstTS => TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(20),
        color: darkGrayColor,
        fontFamily: "montserrat",
      );

  TextStyle get darkGray12BOldMonstTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(12),
        color: darkGrayColor,
        fontFamily: "montserrat",
      );

  TextStyle get black20BoldMonstTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(20),
        color: blackColor,
        fontFamily: "montserrat",
      );

  get white20RegMonstTS => TextStyle(
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.none,
        fontSize: getResponsiveFont(20),
        color: whiteColor,
        fontFamily: "montserrat",
      );

  TextStyle get white21RegMonstTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(21),
        color: Colors.white,
        fontFamily: "montserrat",
        decoration: TextDecoration.none,
      );

  ///
  ///  - family: quicksand
  ///      fonts:
  ///        - asset: assets/fonts/Quicksand-Bold.ttf
  ///          weight: 700
  ///        - asset: assets/fonts/Quicksand-Regular.ttf
  ///          weight: 400
  ///        - asset: assets/fonts/Quicksand-Light.ttf
  ///          weight: 300

  TextStyle get lightGray16RegQSTS => TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(16),
        color: lightGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get yellow20BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(20),
        color: yellowColor,
        fontFamily: "quicksand",
      );

  TextStyle get dullWhite14RegQSTS => TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(14),
        color: dullWhiteColor,
        fontFamily: "quicksand",
      );

  TextStyle get lightGray18BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(18),
        color: lightGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get darkGrey21BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(21),
        color: darkGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get white52BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(52),
        color: whiteColor,
        fontFamily: "quicksand",
      );

  TextStyle get pink20BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(20),
        color: pinkFontColor,
        fontFamily: "quicksand",
      );

  TextStyle get pink18BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(18),
        color: pinkFontColor,
        fontFamily: "quicksand",
      );

  TextStyle get pink14BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(14),
        color: pinkFontColor,
        fontFamily: "quicksand",
      );

  get pink15BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(15),
        color: hungryScanuttTextColor,
        fontFamily: "quicksand",
      );

  get pink10BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(10),
        color: hungryScanuttTextColor,
        fontFamily: "quicksand",
      );

  TextStyle get orange14BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(14),
        color: orangeFontColor,
        fontFamily: "quicksand",
      );

  TextStyle get lightGray14RegQSTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(14),
        color: lightGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get lightGray14BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(14),
        color: lightGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get darkGray14BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(14),
        color: darkGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get darkGray15BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(15),
        color: darkGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get darkGray25BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(25),
        color: darkGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get gray12RegQSTS => TextStyle(
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.none,
        fontSize: getResponsiveFont(12),
        color: lightGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get white20BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(20),
        color: whiteColor,
        fontFamily: "quicksand",
      );

  TextStyle get white17BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(17),
        color: whiteColor,
        fontFamily: "quicksand",
      );

  TextStyle get white14BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(14),
        color: whiteColor,
        fontFamily: "quicksand",
      );

  TextStyle get white14RegQSTS => TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(14),
        color: whiteColor,
        fontFamily: "quicksand",
      );

  TextStyle get white12RegQSTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(14),
        color: whiteColor,
        fontFamily: "quicksand",
      );

  TextStyle get white16BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(16),
        color: whiteColor,
        fontFamily: "quicksand",
      );

  TextStyle get lightGrey12RegQSTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(12),
        color: lightGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get lightGray12BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(12),
        color: lightGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get lightGray9BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(9),
        color: lightGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get white18BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(18),
        color: whiteColor,
        fontFamily: "quicksand",
      );

  TextStyle get lightGray16BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(16),
        color: lightGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get darkGrey21RegQSTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(21),
        color: darkGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get darkGrey14RegQSTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(14),
        color: darkGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get lightGrey9RegQSTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(9),
        color: lightGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get lightGrey10BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(10),
        color: lightGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get lightOrange20RegQSTS =>
      TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(20),
        color: lightOrangeFontColor,
        fontFamily: "quicksand",
      );

  TextStyle get darkGrey18BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(18),
        color: darkGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get darkGrey12BoldQSTS => TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(12),
        color: darkGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get darkGrey11BoldQSTS => TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(11),
        color: darkGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get darkGray14RegQSTS => TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: getResponsiveFont(14),
        color: darkGrayColor,
        fontFamily: "quicksand",
      );


  TextStyle get unSelectedTextColor14BoldQSTS =>
      TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: getResponsiveFont(14),
        color: unSelectedTextColor,
        fontFamily: "quicksand",
      );

  TextStyle get darkGrey16LightQSTS =>
      TextStyle(
        fontWeight: FontWeight.w300,
        fontSize: getResponsiveFont(16),
        color: darkGrayColor,
        fontFamily: "quicksand",
      );

  TextStyle get lightGrey16LightQSTS =>
      TextStyle(
        fontWeight: FontWeight.w300,
        fontSize: getResponsiveFont(16),
        color: lightGrayColor,
        fontFamily: "quicksand",
      );

  BoxDecoration get gradientBackground =>
      BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            loginStartGradient,
            loginEndGradient,
          ],
        ),
      );

  BoxDecoration get gradientThreeColorButtonBackground =>
      BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [
              loginStartGradient,
              loginCenterGradient,
              loginEndGradient,
            ],
          ),
          borderRadius: BorderRadius.circular(getResponsiveWidth(100)));

  BoxDecoration get gradientTwoColorButtonBackground =>
      BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [orangeStartGradient, orangeEndGradient],
          ),
          borderRadius: BorderRadius.circular(getResponsiveWidth(100)));

  BoxDecoration get gradientOrangeBackground =>
      BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [cardEndGradient, cardStartGradient],
          ),
          borderRadius: BorderRadius.circular(getResponsiveWidth(100)));

  BoxDecoration get gradientBlueBackground =>
      BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [cardParticleEndGradient, cardParticleStartGradient],
          ),
          borderRadius: BorderRadius.circular(getResponsiveWidth(100)));


  @override
  Widget build(BuildContext context) {
    return _InheritedStateContainer(
      data: this,
      child: widget.child,
    );
  }
}

class _InheritedStateContainer extends InheritedWidget {
  final AppThemeState data;

  _InheritedStateContainer({
    Key key,
    @required this.data,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedStateContainer old) => true;
}
