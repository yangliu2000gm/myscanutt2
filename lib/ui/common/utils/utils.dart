import 'dart:io';

import 'package:myscanutt2/ui/common/utils/app_images.dart';
import 'package:myscanutt2/ui/common/utils/app_theme.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Utils {
  static final Utils instance = Utils();

  static const animationDuration = Duration(milliseconds: 200);

  static Future<String> getDeviceId() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  /*static Future<String> getFirebaseToken() async {
    FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    return await _firebaseMessaging.getToken();
  }*/

  static Widget getScanuttImage(AppThemeState _appTheme, String image) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          width: _appTheme.getResponsiveWidth(203),
          height: _appTheme.getResponsiveWidth(204),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _appTheme.whiteColor.withOpacity(0.26)),
        ),
        Container(
          width: _appTheme.getResponsiveWidth(180),
          height: _appTheme.getResponsiveWidth(180),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _appTheme.whiteColor.withOpacity(0.93)),
        ),
        Container(
          width: _appTheme.getResponsiveWidth(146),
          height: _appTheme.getResponsiveWidth(157),
          child: Image.asset(
            image,
            fit: BoxFit.fill,
          ),
        ),
      ],
    );
  }

  static Widget getScanutt263Image(AppThemeState _appTheme) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          width: _appTheme.getResponsiveWidth(263),
          height: _appTheme.getResponsiveHeight(263),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _appTheme.whiteColor.withOpacity(0.26)),
        ),
        Container(
          width: _appTheme.getResponsiveWidth(235),
          height: _appTheme.getResponsiveHeight(235),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _appTheme.whiteColor.withOpacity(0.93)),
        ),
        Container(
          width: _appTheme.getResponsiveWidth(188),
          height: _appTheme.getResponsiveHeight(201),
          child: Image.asset(
            AppImages.scannut,
            fit: BoxFit.fill,
          ),
        ),
      ],
    );
  }

  static Widget getHungaryScanuttImage(AppThemeState _appTheme) {
    return Center(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            width: _appTheme.getResponsiveWidth(261),
            height: _appTheme.getResponsiveWidth(260),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _appTheme.whiteColor.withOpacity(0.26)),
          ),
          Container(
            width: _appTheme.getResponsiveWidth(233),
            height: _appTheme.getResponsiveWidth(223),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _appTheme.whiteColor.withOpacity(0.93)),
          ),
          Container(
            margin: EdgeInsets.only(left: _appTheme.getResponsiveWidth(25)),
            width: _appTheme.getResponsiveWidth(190),
            height: _appTheme.getResponsiveWidth(190),
            child: Image.asset(
              AppImages.hungryScanutt,
              fit: BoxFit.fill,
            ),
          ),
        ],
      ),
    );
  }

  static Widget getScanutt126SizeImage(AppThemeState _appTheme) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          width: _appTheme.getResponsiveWidth(126),
          height: _appTheme.getResponsiveWidth(126),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _appTheme.whiteColor.withOpacity(0.26)),
        ),
        Container(
          width: _appTheme.getResponsiveWidth(112),
          height: _appTheme.getResponsiveWidth(112),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _appTheme.whiteColor.withOpacity(0.93)),
        ),
        Container(
          width: _appTheme.getResponsiveWidth(73),
          height: _appTheme.getResponsiveHeight(78),
          child: Image.asset(
            AppImages.scanuttHello,
            fit: BoxFit.fill,
          ),
        ),
      ],
    );
  }

  static Widget getBackgroundImage(Size mediaQuerySize) {
    return Container(
        height: mediaQuerySize.height,
        width: mediaQuerySize.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.backgroundImage), fit: BoxFit.cover),
        ));
  }

  static Widget getCircleLayout(
      double outerCircleWidth,
      double outerCircleHeight,
      double innerCircleWidth,
      double innerCircleHeight,
      Color outerColor,
      Color innerColor,
      Widget widget,
      AppThemeState _appTheme,
      double margin) {
    return Stack(
      children: [
        Container(
          height: _appTheme.getResponsiveHeight(outerCircleHeight),
          width: _appTheme.getResponsiveWidth(outerCircleWidth),
          decoration: BoxDecoration(
            color: outerColor,
            shape: BoxShape.circle,
          ),
        ),
        Positioned(
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          child: Container(
            margin: EdgeInsets.all(margin),
            height: innerCircleHeight,
            width: innerCircleWidth,
            decoration: BoxDecoration(
              color: innerColor,
              shape: BoxShape.circle,
            ),
          ),
        ),
        Positioned(
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          child: widget,
        ),
      ],
    );
  }

  static Widget getRectangleLayout(
    double outerRectangleWidth,
    double outerRectangleHeight,
    double innerRectangleWidth,
    double innerRectangleHeight,
    Color outerColor,
    Color innerColor,
    Widget widget,
    AppThemeState _appTheme,
    double topMargin,
    double bottomMargin,
    double leftMargin,
    double rightMargin,
  ) {
    return Stack(
      children: [
        Container(
          height: _appTheme.getResponsiveHeight(outerRectangleHeight),
          width: _appTheme.getResponsiveWidth(outerRectangleWidth),
          decoration: BoxDecoration(
              color: outerColor,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(40.0)),
        ),
        Positioned(
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          child: Container(
            margin: EdgeInsets.fromLTRB(
                leftMargin, topMargin, rightMargin, bottomMargin),
            height: innerRectangleHeight,
            width: innerRectangleWidth,
            decoration: BoxDecoration(
                color: innerColor,
                shape: BoxShape.rectangle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    blurRadius: 3.0,
                    spreadRadius: 5.0,
                    offset: Offset(0.2, 0.2), // shadow direction: bottom right
                  )
                ],
                borderRadius: BorderRadius.circular(40.0)),
            child: widget,
          ),
        ),
//        Positioned(
//          left: 0,
//          right: 0,
//          top: 0,
//          bottom: 0,
//          child: widget,
//        ),
      ],
    );
  }

  static Widget getBasketImage(AppThemeState _appTheme) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          width: _appTheme.getResponsiveWidth(200),
          height: _appTheme.getResponsiveWidth(201),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _appTheme.whiteColor.withOpacity(0.26)),
        ),
        Container(
          width: _appTheme.getResponsiveWidth(180),
          height: _appTheme.getResponsiveWidth(179),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _appTheme.whiteColor.withOpacity(0.93)),
        ),
        Container(
          width: _appTheme.getResponsiveWidth(93),
          height: _appTheme.getResponsiveHeight(94),
          child: Image.asset(
            AppImages.basket,
            fit: BoxFit.fill,
          ),
        ),
      ],
    );
  }
}
