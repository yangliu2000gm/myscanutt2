class StringConstants {
  static const String noInternetConnection =
      "Please check your Internet connection";
  static const String registerLogin = "Register/login";
  static const String introText1 =
      "On the way back to galaxy - Centurion on a scouting mission, Scanutt’s ship have encountered a nasty asteriod storm";
  static const String letsBegin = "LET’S BEGIN";
  static const String checkIn = "Click to Check In";
  static const String wantToKnow = "Scanutt wants to know more about you";
  static const String dob = "Date of Birth";

  static const String next = "Next";
  static const String agreeTerms = "I agree to the \“TERMS & CONDITIONS \”";
  static const String coivdTerms =
      "I agree to the covid-19 feature terms and conditions.";
  static const String hiIAm = "Hi, I am ";
  static const String scanutt = "Scanutt \n";

  static const String thankYou = "Thank you for saving me";

  static const String skip = "Skip";
  static const String iAmFrom =
      "I am from planet Scallion. I would like to be your friend, and to know more about ";
  static const String planet = "PLANET EARTH ";
  static const String and = "and ";
  static const String you = "YOU";
  static const String bringMe =
      "Bring me wherever you go, and when you need to checkin for ";
  static const String covidTracking = "COVID TRACING ";
  static const String pleaseUse = ", please use the ";
  static const String scan = "SCAN ";
  static const String buttonInstead = "button here instead of your camera  ";
  static const String byScanning =
      "By scanning with this Scan button, mysterious and interesting things will appear. Have fun exploring!";
  static const String pleaseLogin =
      "Please login to your social account so that our experiences are remembered.";

  static const String enterBirthDate =
      "I am excited to know more about you! Please enter your birth date.";
  static const String welcome = "Welcome";
  static const String hungryScanut = "Scanutt is Hungry";
  static const String particleStore = "Particle Store";
  static const String particleSubTitle = "Get things for Scanutt";
  static const String infinityPouch = "INFINITY POUCH";
  static const String infinityPouchSubTitle = "Scanutt’s Collections";
  static const String greetings = "Good Morning, Sam";
  static const String yourProfile = "Your profile";
  static const String currentLevel = "Current Level: 1 ";
  static const String getMoreCoins = "get more rubicoins";
  static const String buyMore = "Buy More";
  static const String buy = "Buy";
  static const String notificationCard = "NOTIFICATION CARD";
  static String dummyText = "Lorem ipsum Lorem ipsum";
  static String dummyText1 =
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum";
  static String itemsInsideTheBasket = "Your items inside the basket";
  static String breakfast = "Breakfast";
  static const String home = "Home";
  static const String profile = "Profile";
  static String scanMe = "Scan Me";
  static String dummyLevel = "Nasi Lemak Rendang";
  static String x20 = "X20";
  static String fullness = "Fullness";
  static String dummyPercentage = "45%";
  static const String feedMe = "Feed Me";

  static const String playWithMe = "Play with me";
  static const String food = "Food";
  static const String toys = "Toys";
  static const String iAm = "I am: ";
  static const String hungry = "\"Hungry\"";

  static const String letsGoOut = "Lets go out and play";

  static const String myStatus = "My Status:";

  static const String happiness = "Happiness:";

  static const String eightyPercentage = "80%";
  static const String sixtyPercentage = "60%";
  static const String twentyPercentage = "20%";
  static const String friendship = "Friendship:";
  static const String menu2 = "Menu2";
  static const String menu3 = "Menu3";
  static const String iceCream = "ICE-CREAM";
  static const String happinessWithoutColon = "Happiness";

  static const String fullness_colon = "Fullness:";
  static const String percentage25 = "25%";
  static const String percentage100 = "100%";
  static const String buyFood =
      "Scanutt has nothing to eat because you have no food in the basket.\nPlease buy some food now to feed Scanutt.";
  static const String level1 = "Level 1";

  static const String whatYouGetToday = "What can i get you today?";
  static const String changeCoinsIntoObjects =
      "Our machine changes coins into objects";
  static const String buyScanutFoods =
      "Scanutt loves to eat all sorts of food and drinks. Buy scanutt some food and keep in basket.";

  static const String moreCoins = "More Coins";
  static const String earnMoreCoins = "You can also earn more coins";
  static const String scanuttLikeTo = "Scanutt likes to collect stuff";
  static const String helpScanutt =
      "Help Scanitt know more about earth by collecting stuff for him";

  static const String watchOrShareVideo =
      "Watch a video or share with your friends to earn more.";
  static const String watchVideo = "Watch Video";

  static const String rubiCoin100 = "Rubicoin x100";
  static const String rubiCoin30 = "Rubicoin x30";

  static const String shareWithFriend = "Share with friends";
  static const String collections = "Collections";
  static const String tools = "Tools";
  static const String accountDetails = "Account Details";
  static const String faqs = "FAQS";
  static const String faq = "FAQ";
  static const String termsAndConditions = "Terms and Conditions";
  static const String privacyPolicy = "Privacy Policy";
  static const String logout = "Logout";
  static const String version = "Version 01.003";
  static const String weAreSorry = "We are Sorry!";

  static const String ok = "ok";

  static const String noMoreVideos =
      "Looks like no more videos to watch for today";
  static const String successScan = "Yay, you have successfully scanned";
  static const String checkInInformation =
      "Your checkin information is recorded for covid tracing. A screenshot of the receipt has been stored in your gallery for your further use";
  static const String getMyReward = "Get my reward";

  static const String health = "Health";

  static const String plus20 = "+20";

  static const String yay = "YAAAY !!!! You have received";

  static const String coin = "Coin";

  static const String gravityWall = "GRAVITY WALL";

  static const String yummy = "YUMMY";
  static const String scanuttIsFull = "YAAY!!! Scannut is full now";
  static const String congratulations =
      "Congratulations, you have reached level 7.";
  static const String youHaveReceived = "YAAAY !!!! You have received";
  static const String iceCreamLabel = "ICECREAM";
  static const String iceCreamSubDesc =
      "Includes, health 50%, hungry 20% \nThis is stored in Scanutt's food basket.";
}
