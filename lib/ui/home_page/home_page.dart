import 'package:myscanutt2/ui/common/utils/app_images.dart';
import 'package:myscanutt2/ui/common/utils/app_theme.dart';
import 'package:myscanutt2/ui/common/utils/routes.dart';
import 'package:myscanutt2/ui/common/utils/strings.dart';
import 'package:myscanutt2/ui/home_page/home_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // HomeBloc _homeBloc = HomeBloc();
  AppThemeState _appTheme;

  @override
  Widget build(BuildContext context) {
    _appTheme = AppTheme.of(context);
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        padding: EdgeInsets.only(
            top: _appTheme.getResponsiveHeight(80),
            left: _appTheme.getResponsiveWidth(36),
            right: _appTheme.getResponsiveWidth(36)),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: _getCardsList(),
      ),
    );
  }

  Widget _getCardsList() {
    return NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (OverscrollIndicatorNotification overscroll) {
          overscroll.disallowGlow();
          return false;
        },
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: _appTheme.getResponsiveHeight(30),
              ),
              getGreetingsLabel(),
              getCurrentLevelLabel(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(40),
              ),
              _welComeCard(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(35),
              ),
              _particleStoreCard(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(35),
              ),
              _getInfinityPouchCard(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(35),
              ),
              _getMoreCoinsCard(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(35),
              ),
              _getNotificationCard(),
              SizedBox(
                height: _appTheme.getResponsiveHeight(195),
              ),
            ],
          ),
        ));
  }

  Widget _welComeCard() {
    return GestureDetector(
      onTap: () {
        Navigator.of(context, rootNavigator: false)
            .pushNamed(RouteName.scanuttStatusPage);
      },
      child: Card(
        elevation: 8.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(40.0),
        ),
        child: Container(
            padding: EdgeInsets.only(
                left: _appTheme.getResponsiveWidth(21.5),
                right: _appTheme.getResponsiveWidth(21.5)),
            width: _appTheme.getResponsiveWidth(302),
            height: _appTheme.getResponsiveHeight(120),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40.0),
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: [
                      _appTheme.cardStartGradient,
                      _appTheme.cardEndGradient
                    ])),
            child: Row(
              children: [
                _getScanuttImage(Colors.white.withOpacity(0.4)),
                SizedBox(
                  width: _appTheme.getResponsiveWidth(10),
                ),
                _getWelcomeText(),
              ],
            )),
      ),
    );
  }

  Widget _getScanuttImage(Color color) {
    return Container(
      width: _appTheme.getResponsiveWidth(85),
      height: _appTheme.getResponsiveWidth(85),
      decoration: BoxDecoration(shape: BoxShape.circle, color: color),
      child: Padding(
        padding: EdgeInsets.only(
          top: _appTheme.getResponsiveHeight(7.5),
          bottom: _appTheme.getResponsiveHeight(7.5),
          left: _appTheme.getResponsiveHeight(9.5),
          right: _appTheme.getResponsiveHeight(9.5),
        ),
        child: Image.asset(
          AppImages.scannut,
          width: _appTheme.getResponsiveWidth(66),
          height: _appTheme.getResponsiveWidth(70),
        ),
      ),
    );
  }

  Widget _getWelcomeText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          StringConstants.welcome,
          style: _appTheme.white14BoldQSTS,
        ),
        SizedBox(
          height: _appTheme.getResponsiveHeight(6),
        ),
        Text(
          StringConstants.hungryScanut,
          style: _appTheme.pink15BoldQSTS,
        )
      ],
    );
  }

  Widget _particleStoreCard() {
    return Card(
      elevation: 8.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_appTheme.getResponsiveWidth(45)),
      ),
      child: Container(
          padding: EdgeInsets.only(
              left: _appTheme.getResponsiveWidth(21.5),
              right: _appTheme.getResponsiveWidth(21.5)),
          width: _appTheme.getResponsiveWidth(302),
          height: _appTheme.getResponsiveHeight(120),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40.0),
              // gradient: LinearGradient(
              //     begin: Alignment.bottomCenter,
              //     end: Alignment.topCenter,
              //     colors: [
              //       _appTheme.cardPartStartGradient,
              //       _appTheme.cardParticleEndGradient
              //     ])
                  ),
          child: Row(
            children: [
              _getParticleStoreImage(),
              SizedBox(
                width: _appTheme.getResponsiveWidth(10),
              ),
              _getParticleText(),
            ],
          )),
    );
  }

  Widget _getParticleStoreImage() {
    return Container(
      width: _appTheme.getResponsiveWidth(85),
      height: _appTheme.getResponsiveWidth(85),
      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
      child: Padding(
        padding: EdgeInsets.all(
          _appTheme.getResponsiveHeight(2),
        ),
        child: Image.asset(
          AppImages.particleImage,
        ),
      ),
    );
  }

  Widget _getParticleText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          StringConstants.particleStore,
          style: _appTheme.white16BoldQSTS,
        ),
        SizedBox(
          height: _appTheme.getResponsiveHeight(6),
        ),
        Text(
          StringConstants.particleSubTitle,
          style: _appTheme.white14BoldQSTS,
        )
      ],
    );
  }

  Widget _getInfinityPouchCard() {
    return Card(
      elevation: 8.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_appTheme.getResponsiveWidth(45)),
      ),
      child: Container(
          padding: EdgeInsets.only(
              left: _appTheme.getResponsiveWidth(21.5),
              right: _appTheme.getResponsiveWidth(21.5)),
          width: _appTheme.getResponsiveWidth(302),
          height: _appTheme.getResponsiveHeight(120),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40.0),
              gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    _appTheme.cardInfinityPouchEndGradient,
                    _appTheme.cardInfinityPouchStartGradient
                  ])),
          child: Row(
            children: [
              _getInfinityPouchImage(),
              SizedBox(
                width: _appTheme.getResponsiveWidth(10),
              ),
              _getInfinityPouchText(),
            ],
          )),
    );
  }

  Widget _getInfinityPouchImage() {
    return Container(
      width: _appTheme.getResponsiveWidth(85),
      height: _appTheme.getResponsiveWidth(85),
      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
      child: Padding(
        padding: EdgeInsets.all(
          _appTheme.getResponsiveHeight(8),
        ),
        child: Image.asset(
          AppImages.infinityPouch,
        ),
      ),
    );
  }

  Widget _getInfinityPouchText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          StringConstants.infinityPouch,
          style: _appTheme.white16BoldQSTS,
        ),
        SizedBox(
          height: _appTheme.getResponsiveHeight(6),
        ),
        Text(
          StringConstants.infinityPouchSubTitle,
          style: _appTheme.white14BoldQSTS,
        )
      ],
    );
  }

  Widget getGreetingsLabel() {
    return Text(
      StringConstants.greetings,
      style: _appTheme.white20BoldQSTS,
      textAlign: TextAlign.start,
    );
  }

  Widget getCurrentLevelLabel() {
    return Text(
      StringConstants.currentLevel,
      style: _appTheme.white14RegQSTS,
      textAlign: TextAlign.start,
    );
  }

  Widget _getMoreCoinsCard() {
    return Card(
      elevation: 8.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_appTheme.getResponsiveWidth(45)),
      ),
      child: Container(
          padding: EdgeInsets.only(
              left: _appTheme.getResponsiveWidth(21.5),
              right: _appTheme.getResponsiveWidth(21.5)),
          width: _appTheme.getResponsiveWidth(302),
          height: _appTheme.getResponsiveHeight(120),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40.0),
              gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    _appTheme.moreCoinsEndGradient,
                    _appTheme.moreCoinsStartGradient
                  ])),
          child: Row(
            children: [
              _getMoreCoinsImage(),
              SizedBox(
                width: _appTheme.getResponsiveWidth(10),
              ),
              _getMoreCoinsText(),
            ],
          )),
    );
  }

  Widget _getMoreCoinsImage() {
    return Container(
      width: _appTheme.getResponsiveWidth(85),
      height: _appTheme.getResponsiveWidth(85),
      decoration: BoxDecoration(
          shape: BoxShape.circle, color: Colors.white.withOpacity(0.14)),
      child: Padding(
        padding: EdgeInsets.all(
          _appTheme.getResponsiveHeight(8),
        ),
        child: Image.asset(
          AppImages.getMoreCoins,
        ),
      ),
    );
  }

  Widget _getMoreCoinsText() {
    return Flexible(
      child: Text(
        StringConstants.getMoreCoins.toUpperCase(),
        style: _appTheme.white16BoldQSTS,
      ),
    );
  }

  Widget _getNotificationCard() {
    return Card(
      elevation: 8.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_appTheme.getResponsiveWidth(45)),
      ),
      child: Container(
          padding: EdgeInsets.only(
              left: _appTheme.getResponsiveWidth(21.5),
              right: _appTheme.getResponsiveWidth(21.5)),
          width: _appTheme.getResponsiveWidth(302),
          height: _appTheme.getResponsiveHeight(120),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40.0),
              gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    _appTheme.notificationEndGradient,
                    _appTheme.notificationStartGradient
                  ])),
          child: Row(
            children: [
              _getScanuttImage(Colors.white.withOpacity(0.14)),
              SizedBox(
                width: _appTheme.getResponsiveWidth(10),
              ),
              _getNotificationList(),
            ],
          )),
    );
  }

  Widget _getNotificationList() {
    return Flexible(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              StringConstants.notificationCard,
              style: _appTheme.white16BoldQSTS,
            ),
            Text(
              StringConstants.dummyText,
              style: _appTheme.white14RegQSTS,
            ),
            Text(
              StringConstants.dummyText,
              style: _appTheme.white14RegQSTS,
            ),
          ],
        ),
      ),
    );
  }
}
