import 'package:myscanutt2/ui/common/utils/app_images.dart';
import 'package:myscanutt2/ui/common/utils/app_theme.dart';
import 'package:myscanutt2/ui/common/utils/routes.dart';

import 'package:myscanutt2/ui/common/utils/utils.dart';
import 'package:myscanutt2/ui/common/widgets/custom_app_bar.dart';
import 'package:myscanutt2/ui/common/widgets/custom_bottom_navigation.dart';
import 'package:myscanutt2/ui/common/widgets/slide_page_route.dart';
import 'package:myscanutt2/ui/dashboard/dashboard_bloc.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  var mediaQuerySize;
  AppThemeState _appTheme;
  //DashBoardBloc _dashBoardBloc = DashBoardBloc();
  GlobalKey<NavigatorState> _navigatorKey = GlobalKey();
  var padding;
  GlobalKey<ScaffoldState> _drawerKey = new GlobalKey<ScaffoldState>();
  GlobalKey<CustomAppbarState> _titleKey = new GlobalKey<CustomAppbarState>();

  @override
  Widget build(BuildContext context) {
    _appTheme = AppTheme.of(context);
    padding = MediaQuery.of(context).padding;
    mediaQuerySize = MediaQuery.of(context).size;

    //print("reach dashboartd.dart");
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      extendBodyBehindAppBar: true,
      extendBody: true,
      body: Stack(
        children: [
          Utils.getBackgroundImage(mediaQuerySize),
         Column(
           children: [
             CustomAppbar(
               drawerKey: _drawerKey,
               height: _appTheme.getResponsiveHeight(90),
               key: _titleKey,
             ),
             Expanded(flex: 1, child: _getNavigator()),
           ],
         ),
        ],
      ),
      bottomNavigationBar: CustomNavigationBar(),
    );
  }

  Widget getBackgroundImage() {
    return Container(
        height: mediaQuerySize.height,
        width: mediaQuerySize.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.backgroundImage), fit: BoxFit.cover),
        ));
  }

  Widget _getNavigator() {
    return Navigator(
      // initialRoute: RouteName.emptyBasketPage,
      initialRoute: RouteName.homePage,      
      onGenerateRoute: (RouteSettings settings) {
        Widget widget;
       if (Routes.commonRoutes.containsKey(settings.name)) {
         widget = Routes.commonRoutes[settings.name];
         //_titleKey.currentState.updateTitle("hello");
       } else {
         widget = Offstage();
       }

        final _pageRouteBuilder =
            SlidePageRoute(page: widget, settings: settings);
        //_pageRoutes[AppDrawer.currentSelectedKey] = _pageRouteBuilder;
        return _pageRouteBuilder;
      },
    );
  }
}
